/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package da
import scala.xml.Node
import bi.AccessFlags
import bi.AccessFlagsContexts
import br.MethodDescriptor
/**
 * @author Michael Eichberg
 * @author Wael Alkhatib
 * @author Isbel Isbel
 * @author Noorulla Sharief
 */
case class Method_Info(
        accessFlags: Int,
        name_index: Constant_Pool_Index,
        descriptor_index: Constant_Pool_Index,
        attributes: Attributes) {

    def toXHTML(implicit cp: Constant_Pool): Node = {     
        <div class="methodinfo" title= {cp(name_index).toString(cp)} 
        name={ AccessFlags.toString(accessFlags, AccessFlagsContexts.METHOD ) }>
            <div class="code">
                <span class="AccessFlags">{ AccessFlags.toString(accessFlags, AccessFlagsContexts.METHOD ) }</span>
                <span>{namedParameters(MethodDescriptor(cp(descriptor_index).asString)
                        .toJava(cp(name_index).asString))}</span>
                <a href="#" class="tooltip">{ name_index } <span>{ cp(name_index) }</span></a> 
                {attributesToXHTML}
            </div>
       </div>
    }

    def namedParameters(methode_Descriptor:String) :String= {
        var splitted=methode_Descriptor.split('(')
        val namedparameters=new StringBuilder(splitted(0)+"(");       
        if(!splitted(1).equals(")"))
        {
            splitted=splitted(1).split(')')
            val parmeters=splitted(0).split(',')      
            for { number ← (0 until parmeters.length) } yield 
            {
                namedparameters.append(parmeters(number)+" arg"+number)
                if(parmeters.length>1 && number<parmeters.length-1)
                {
                    namedparameters.append(",")
                }
            }
        }
        namedparameters+")";
    }
    
    def attributesToXHTML(implicit cp: Constant_Pool) = {
        for (attribute ← attributes) yield attribute.toXHTML(cp)
    }
}